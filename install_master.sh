#!/bin/bash

# Install docker swarm

echo "END - install master - "$IP

# Initialisation d'un cluster swarm.
# Le noeud courant sur lequel nous allons lancer la commande est considere
# comme le manager

#IP=`arp $(hostname) | grep "192.168" | awk -F " " '{print$1}'`
# IP=`ifconfig -a | awk '$1 == "inet" {print $2}' | grep "192.168"`
# sudo  docker swarm init --advertise-addr $IP > /tmp/swarm_join.txt

# Nous recuperons dans le script install_node.sh les commandes indiquees
# dans le resultat de la commande docker swarm
#
# root@kmaster1:/home/vagrant# docker swarm init --advertise-addr 192.168.100.11
# Swarm initialized: current node (qb92di05oywh2c9obpfkq8ldc) is now a manager.
#
# To add a worker to this swarm, run the following command:
#
#   docker swarm join --token SWMTKN-1-4glniye8nv83c8m2l8j4z55qg0b76rvxvcafj62gbjysnwzqbn-e063ybh54bhlhww9b3l87bdk4 192.168.100.11:2377
#
# To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
#
# root@kmaster1:/home/vagrant# 
#
# add_worker=`grep "--token" /tmp/swarm_join.txt
# add_manager=`grep "join-token" /tmp/swarm_join.txt

# Install docker-compose
# sudo apt-get install -y docker-compose

# Install sshpass
echo "install sshpass"
sudo apt-get install sshpass

# Install net-tools
echo "install net-tools"
sudo apt install net-tools

# Install ansible
echo "install ansible"
#sudo apt update -y
sudo apt --fix-broken install
sudo apt install -y ansible

# Creer un utilisateur ansible
sudo useradd ansible --create-home -s /bin/bash

# Inserer manuellement l utilisateur ansible dans le sudoer
# echo "Inserer la ligne suivante dans le /etc/sudoers :"
sudo echo "ansible ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Creer le repertoire Ansible
sudo mkdir /etc/ansible
sudo chown -R ansible:ansible /etc/ansible

echo "END - install master - "$IP
