#!/bin/bash

## Install common for k8s

HOSTNAME=$(hostname)
IP=$(hostname -I | awk '{print $2}')
echo "START - install common - "$IP

echo "[1]: add host name for ip"
host_exist=$(cat /etc/hosts | grep -i "$IP" | wc -l)
if [ "$host_exist" == "0" ]; then
    echo "$IP $HOSTNAME" >>/etc/hosts
fi

echo "[2]: disable  swap"
# swapoff - a to disable swapping
swapoff -a
# sed to comment the swap partition in /etc/fstab
sed -i.bak -r 's/(.+ swap .+)/#\1/' /etc/fstab

echo "[3]: install docker"
# Mise a jour de la liste des packages existants
sudo apt -y update
sudo apt -y upgrade
# Installer quelques paquets pre-requis qui permettent a apt d utiliser
# les paquets sur HTTPS
sudo apt-get install -y curl apt-transport-https ca-certificates software-properties-common
# Ajouter la cle GPG du depot officiel de Docker a votre systeme
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
# Ajouter le referentiel Docker aux sources APT
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
# Mise a jour de la base de donnees des paquets avec les paquets Docker a partir du referentiel
# qui vient d etre ajoute
sudo apt -y update
# Verifier que vous etes sur le point d installer a partir du depot Docker et non du depot ubuntu
# par defaut
sudo apt-cache policy docker-ce
# Install Docker
sudo apt install -y docker-ce
# Verifier que Docker est demarre
sudo systemctl status docker
# Pour eviter de taper sudo chaque fois que vous executez la commande docker, ajouter votre
# nom d utilisateur au groupe docker
sudo usermod -aG docker vagrant

echo "[5]: add tools"
sudo apt-get install -y vim
sudo apt-get install -y net-tools

echo "END - install common - " $IP
