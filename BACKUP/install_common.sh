#!/bin/bash

## Install common for k8s

HOSTNAME=$(hostname)
IP=$(hostname -I | awk '{print $2}')
echo "START - install common - "$IP

echo "[1]: add host name for ip"
host_exist=$(cat /etc/hosts | grep -i "$IP" | wc -l)
if [ "$host_exist" == "0" ]; then
    echo "$IP $HOSTNAME" >>/etc/hosts
fi

echo "[2]: disable  swap"
# swapoff - a to disable swapping
swapoff -a
# sed to comment the swap partition in /etc/fstab
sed -i.bak -r 's/(.+ swap .+)/#\1/' /etc/fstab

echo "[3]: install docker"
sudo apt -y update
sudo apt -y upgrade
sudo apt-get install -y curl apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt -y update
sudo apt install -y docker-ce
sudo usermod -aG docker vagrant

echo "[5]: add tools"
sudo apt-get install -y vim
sudo apt-get install -y net-tools

echo "END - install common - " $IP
