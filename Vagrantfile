# Initialization Vagrant configuration. The paremeter "2" specifies the version of the Vagrant configuration.
# That ensures that the configuration will be interpreted consistently and compatible with the version of Vagrant.

    # Before you must install these plugins to speed up vagrant provisioning :
    # vagrant plugin install vagrant-faster
    # vagrant plugin install vagrant-cachier

Vagrant.configure("2") do |config|

    config.cache.auto_detect = true

    # Variables
    common_packages = "unzip iftop curl software-properties-common git vim tree net-tools telnet"
    numberMaster = 2
    numberSlave=3 

    # master server
    
    # List of nodes
    NODES = (1..numberMaster).map do |n|
        {
            :hostname => "master#{n}",
            :ip => "192.168.56.1#{n}",
            :cpus => 2,
            :mem => 2048
        }
    end

    # Run installation
    NODES.each do |node|
        config.vm.define node[:hostname] do |machine|
            machine.vm.hostname = node[:hostname]
            machine.vm.box = "generic/ubuntu2204"   
            machine.vm.box_url = "generic/ubuntu2204"
            machine.vm.network :private_network, ip: node[:ip]
            machine.vm.provider "virtualbox" do|vm|
                vm.customize ["modifyvm", :id, "--name", node[:hostname]]
                vm.customize ["modifyvm", :id, "--cpus", node[:cpus]]
                vm.customize ["modifyvm", :id, "--memory", node[:mem]]
                vm.customize ["modifyvm", :id, "--hwvirtex", "off"]
                vm.customize [ "modifyvm", :id, "--natdnshostresolver1", "on" ]
                vm.customize [ "modifyvm", :id, "--natdnsproxy1", "on" ]
                vm.customize [ "modifyvm", :id, "--ioapic", "on" ]
                vm.customize [ "modifyvm", :id, "--nictype1", "virtio" ]
            end
            machine.vm.provision "shell", inline: <<-EOF
            sudo apt update -qq 2>&1 >/dev/null
            sudo apt install -y -qq #{common_packages} 2>&1 >/dev/null
            sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
            service ssh restart
EOF
    
        # Additional scripts need to be run to configure the vm after it is created
        # config.vm.provision "shell", path: "install_common.sh"   
        # config.vm.provision "shell", path: "install_master.sh" 
        end
    end

    # slave server
    
    # List of nodes
    NODES2 = (1..numberSlave).map do |n|
        {
            :hostname => "slave#{n}",
            :ip => "192.168.57.1#{n}",
            :cpus => 2,
            :mem => 2048
        }
    end

    NODES2.each do |node|
        config.vm.define node[:hostname] do |machine|
            machine.vm.hostname = node[:hostname]
            machine.vm.box = "generic/ubuntu2204"   
            machine.vm.box_url = "generic/ubuntu2204"
            machine.vm.network :private_network, ip: node[:ip]
            machine.vm.provider "virtualbox" do|vm|
                vm.customize ["modifyvm", :id, "--name", node[:hostname]]
                vm.customize ["modifyvm", :id, "--cpus", node[:cpus]]
                vm.customize ["modifyvm", :id, "--memory", node[:mem]]
                vm.customize ["modifyvm", :id, "--hwvirtex", "off"]
                vm.customize [ "modifyvm", :id, "--natdnshostresolver1", "on" ]
                vm.customize [ "modifyvm", :id, "--natdnsproxy1", "on" ]
                vm.customize [ "modifyvm", :id, "--ioapic", "on" ]
                vm.customize [ "modifyvm", :id, "--nictype1", "virtio" ]
            end
            config.vm.provision "shell", inline: <<-EOF
            sudo apt update -qq 2>&1 >/dev/null
            sudo apt install -y -qq #{common_packages} 2>&1 >/dev/null
            sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
            service ssh restart
EOF
    
        # Additional  scripts need to be run to configure the vm after it is created :
        # slave.vm.provision "shell", path: "install_common.sh"   
        # slave.vm.provision "shell", path: "install_master.sh" 
        end
    end

end
